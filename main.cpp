#include <QCoreApplication>
#include <QDebug>

#include "server.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Server *myserver = new Server();
    myserver->setHost("0.0.0.0");
    myserver->setPort(3000);
    auto ok = myserver->listen();
    qDebug() << ok;

    return a.exec();
}
