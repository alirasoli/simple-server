#include "parser.h"
#include "server.h"

#include <QTcpSocket>

Server::Server(QObject *parent) : QObject(parent)
{
}

Server::~Server()
{
    delete server;
}

void Server::setHost(const QString &host)
{
    _host = host;
}

QString Server::host() const
{
    return _host;
}

void Server::setPort(quint16 port)
{
    _port = port;
}

quint16 Server::port() const
{
    return _port;
}

bool Server::listen()
{
    server = new QTcpServer;
    connect(server, &QTcpServer::newConnection, this, &Server::newConnection);
    connect(server, &QTcpServer::newConnection, this, &Server::createNewConnection);

    if(server->listen(QHostAddress(_host), _port)) {
        emit started();
        return true;
    } else {
        emit failed();
        return false;
    }
}

void Server::createNewConnection()
{
    QTcpSocket *socket = server->nextPendingConnection();
    sockets.insert(socket, "");

    connect(socket, &QTcpSocket::readyRead, this, &Server::readData);
    connect(socket, &QTcpSocket::disconnected, this, &Server::removeSocket);
    qDebug() << "number of connected devices: " << sockets.size();
}

void Server::readData()
{
    QByteArray data = static_cast<QTcpSocket*>(QObject::sender())->readAll();
    Parser req(data);

    switch (req.getCode()) {
    qDebug() << req.getCode();
    case Parser::SetName:
        setName(QObject::sender(), req.getContent());
        break;
    case Parser::PublicMessage:
        sendPublicMessage(QObject::sender(), req.getContent());
        break;
    }
}

void Server::removeSocket()
{
    QTcpSocket *senderSocket = static_cast<QTcpSocket*>(QObject::sender());
    sockets.remove(senderSocket);
    senderSocket->deleteLater();
}

void Server::setName(QObject *sender, const QByteArray &content)
{
    QTcpSocket *senderSocket = static_cast<QTcpSocket*>(sender);
    sockets[senderSocket] = QString(content);
    sendPublicMessage(senderSocket, QString(content + " joined. Number of online users: " + QString::number(sockets.size())).toUtf8());
}

void Server::sendMessage(QObject *receiver, const QByteArray &message)
{
    QTcpSocket *senderSocket = static_cast<QTcpSocket*>(receiver);
    Parser response(Parser::Message, message);

    senderSocket->write(response.getRequest());
}

void Server::sendPublicMessage(QObject *sender, const QByteArray &content)
{
    QTcpSocket *senderSocket = static_cast<QTcpSocket*>(sender);
    QString senderName = sockets[senderSocket];
    QByteArray message = senderName.toUtf8() + ": " + content;

    for(auto socket : sockets.keys()) {
        if(socket == senderSocket) continue;
        sendMessage(socket, message);
    }
}

