#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>

class Server : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString host READ host WRITE setHost NOTIFY hostChanged)
    Q_PROPERTY(quint16 port READ port WRITE setPort NOTIFY portChanged)

public:
    explicit Server(QObject *parent = nullptr);
    ~Server();

public Q_SLOTS:
    void setHost(const QString &host);
    QString host() const;

    void setPort(quint16 port);
    quint16 port() const;

    bool listen();

signals:
    void hostChanged();
    void portChanged();
    void started();
    void failed();
    void newConnection();

private:
    void createNewConnection();
    void readData();
    void removeSocket();

    void setName(QObject *sender, const QByteArray &content);
    void sendPublicMessage(QObject *sender, const QByteArray &content);
    void sendMessage(QObject *receiver, const QByteArray &message);

private:
    QString _host;
    quint16 _port;
    QTcpServer *server;
    QMap<QTcpSocket*, QString> sockets;
};

#endif // SERVER_H
